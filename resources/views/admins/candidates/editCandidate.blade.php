@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Edit Candidate</h1>
@stop

@section('content')
    <form action="{{Route('updateCandidate',['id'=>$users->id])}}" method="post">
        @csrf
        @method('put')
        <label>Name</label></br>
        <input type="text" name="name" id="name" value="{{$users->name}}" class="form-control"></br>
        @error('name')
        <span style="color:red;">{{$message}}</span>
        @enderror
        <label>Email</label></br>
        <input type="email" name="email" id="email" value="{{$users->email}}" class="form-control"></br>
        @error('email')
        <span style="color:red;">{{$message}}</span>
        @enderror
        <label>Pass</label></br>
        <input type="password" name="pass" id="pass" value="{{$users->password}}" class="form-control"></br>
        @error('password')
        <span style="color:red;">{{$message}}</span>
        @enderror
        <input type="submit" value="Save" class="btn btn-success btn-sm" class="form-control"></br>
    </form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop