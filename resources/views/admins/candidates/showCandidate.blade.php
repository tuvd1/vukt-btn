@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Candidate Profile</h1>
@stop

@section('content')
    <h5 class="title">Name : {{ $candidates->name}}</h5>
    <p class="text">Email : {{$candidates->email}}</p>
    <p class="text">File : {{$candidates->file}}</p>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop