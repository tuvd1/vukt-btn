@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>List Candidates</h1>
@stop

@section('content')
    <a href="{{Route('createCandidate')}}" class="btn btn-success btn-sm" title="Add New Candidate">
        Add New Candidate
    </a>
    <a href="{{Route('home')}}" class="btn btn-success btn-sm" title="List Admin">
        List Admin
    </a>
    <a href="{{Route('homeEmployer')}}" class="btn btn-success btn-sm" title="List Employer">
        List Employer
    </a>
    <br/>
    <br/>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>File</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($candidates as $item)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{$item->file}}</td>
                    <td>
                        <a href="{{Route('showCandidate',['id'=>$item->id])}}" title="View Candidate"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true">View</i></button>
                        <a href="{{Route('editCandidate',['id'=>$item->id])}}" title="Edit Candidate"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true">Edit</i></button>
                        <form method="POST" action="{{Route('admin.candidate.delete',['id'=>$item->id])}}" accept-charset="UTF-8" style="display:inline">
                            @method('DELETE')
                            @csrf
                            <button title="Delete Candidate" onclick="return confirm('Confirm delete?')" type="submit" class="btn-danger btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true">Delete</i></button>
                        </form>    
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop