@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>List Employers</h1>
@stop

@section('content')
    <a href="{{Route('createEmployer')}}" class="btn btn-success btn-sm" title="Add New Employer">
        Add New Employer
    </a>
    <a href="{{Route('home')}}" class="btn btn-success btn-sm" title="List Admin">
        List Admin
    </a>
    <a href="{{Route('homeCandidate')}}" class="btn btn-success btn-sm" title="List Candidate">
        List Candidate
    </a>
    <br/>
    <br/>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Content</th>
                    <th>Coin</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($employers as $item)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{$item->content}}</td>
                    <td>{{$item->coin}}</td>
                    <td>
                        <a href="{{Route('showEmployer',['id'=>$item->id])}}" title="View Employer"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true">View</i></button>
                        <a href="{{Route('editEmployer',['id'=>$item->id])}}" title="Edit employer"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true">Edit</i></button>
                        <form method="POST" action="{{Route('admin.employer.delete',['id'=>$item->id])}}" accept-charset="UTF-8" style="display:inline">
                            @method('DELETE')
                            @csrf
                            <button title="Delete Employer" onclick="return confirm('Confirm delete?')" type="submit" class="btn-danger btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true">Delete</i></button>
                        </form>    
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop