@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Employer Profile</h1>
@stop

@section('content')
    <h5 class="title">Name : {{ $employers->name}}</h5>
    <p class="text">Email : {{$employers->email}}</p>
    <p class="text">Content : {{$employers->content}}</p>
    <p class="text">Coin : {{$employers->coin}}</p>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop