@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Admin Profile</h1>
@stop

@section('content')
    <h5 class="title">Name : {{ $users->name}}</h5>
    <p class="text">Email : {{$users->email}}</p>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop