@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Create New Admin</h1>
@stop

@section('content')
    <form action="{{Route('store')}}" method="post">
        @csrf
        @method('put')
        <label>Name</label></br>
        <input type="text" name="name" id="name" class="form-control" placeholder="Name --"/ value="{{old('name')}}"></br>
        @error('name')
        <span style="color:red;">{{$message}}</span>
        @enderror
        <label>Email</label></br>
        <input type="email" name="email" id="email" class="form-control" placeholder="Email --"/ value="{{old('email')}}"></br>
        @error('email')
        <span style="color:red;">{{$message}}</span>
        @enderror
        <label>Pass</label></br>
        <input type="password" name="password" id="password" class="form-control" placeholder="Pass --"/ value="{{old('password')}}"></br>
        @error('password')
        <span style="color:red;">{{$message}}</span>
        @enderror
        <input type="submit" value="Save" class="btn btn-success btn-sm" class="form-control"></br>
    </form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop