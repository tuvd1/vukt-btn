<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @param  string|null  ...$guards
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        return redirect(RouteServiceProvider::HOME);
        $guards = empty($guards) ? [null] : $guards;
        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                if (!Auth::user()){
                    return redirect()->route('login');
                }
                if (Auth::user()->userType === 'ADM'){
                    
                    return redirect()->route('admin');
                }
                if (Auth::user()->userType === 'CDD'){
                    return redirect()->route('candidate');
                }
                if (Auth::user()->userType === 'EPL'){
                    return redirect()->route('employer');
                }
                return redirect(RouteServiceProvider::HOME);
            }
        }
        return $next($request);
    }
}
