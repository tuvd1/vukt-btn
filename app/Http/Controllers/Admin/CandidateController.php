<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Models\User;

use App\Http\Controllers\Controller;

use App\Http\Requests\AdminRequest;


class CandidateController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = User::all();
        return view ('admins.candidates.homeCandidate')->with('users',$users);
    }
      /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admins.candidates.createCandidate');
    }
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminRequest $request)
    {
        $input = $request->all();
        User::create($input);
        return redirect(Route('homeCandidate'))->with('flash_message', 'Candidate Added!');
    }
     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User::find($id);
        return view ('admins.candidates.showCandidate')->with('users',$users);
    }
    public function edit($id)
    {
        $users = User::find($id);
        return view ('admins.candidates.editCandidate')->with('users',$users);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminRequest $request, $id)
    {
        $users = User::find($id);
        $input = $request->all();
        $users -> update($input);
        return redirect(Route('homeCandidate'))->with('flash_message', 'Candidate Updated!');

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return redirect(Route('homeCandidate'))->with('flash_message', 'Candidate deleted!');
    }
}