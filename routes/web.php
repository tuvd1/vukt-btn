<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Admin\CandidateController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\EmployerController;
use App\Http\Controllers\Candidate\CandidateControllerr;
use App\Http\Controllers\Candidate\EmployerControllerr;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home')->middleware(['guest']);
Route::get('/admin',function(){
    return view('admins.admin');
})->name('admin');
Route::get('/candidate',function(){
    return view('candidate');
})->name('candidate');
Route::get('/employer',function(){
    return view('employer');
})->name('employer');
Auth::routes();
Route::get('/admin', [HomeController::class, 'index'])->name('admin');
Route::get('/admin/create', [HomeController::class, 'create'])->name('create');
Route::put('/admin/store',[HomeController::class, 'store'])->name('store');
Route::get('/admin/show/{id}', [HomeController::class, 'show'])->name('show');
Route::get('/admin/edit/{id}', [HomeController::class, 'edit'])->name('edit');
Route::put('/admin/update/{id}',[HomeController::class, 'update'])->name('update');
Route::delete('/admin/delete/{id}',[HomeController::class, 'destroy'])->name('admin.delete');

Route::get('/admin/homeCandidate', [CandidateController::class, 'index'])->name('homeCandidate');
Route::get('/admin/createCandidate', [CandidateController::class, 'create'])->name('createCandidate');
Route::put('/admin/storeCandidate',[CandidateController::class, 'store'])->name('storeCandidate');
Route::get('/admin/showCandidate/{id}', [CandidateController::class, 'show'])->name('showCandidate');
Route::get('/admin/editCandidate/{id}', [CandidateController::class, 'edit'])->name('editCandidate');
Route::put('/admin/updateCandidate/{id}',[CandidateController::class, 'update'])->name('updateCandidate');
Route::delete('/admin/deleteCandidate/{id}',[CandidateController::class, 'destroy'])->name('admin.candidate.delete');

Route::get('/admin/homeEmployer', [EmployerController::class, 'index'])->name('homeEmployer');
Route::get('/admin/createEmployer', [EmployerController::class, 'create'])->name('createEmployer');
Route::put('/admin/storeEmployer',[EmployerController::class, 'store'])->name('storeEmployer');
Route::get('/admin/showEmployer/{id}', [EmployerController::class, 'show'])->name('showEmployer');
Route::get('/admin/editEmployer/{id}', [EmployerController::class, 'edit'])->name('editEmployer');
Route::put('/admin/updateEmployer/{id}',[EmployerController::class, 'update'])->name('updateEmployer');
Route::delete('/admin/deleteEmployer/{id}',[EmployerController::class, 'destroy'])->name('admin.employer.delete');